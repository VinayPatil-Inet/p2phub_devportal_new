//const https = require('https')
const fs = require('fs')
require('dotenv').config();
const axios = require('axios');
const express = require('express');
const path = require('path');
//const qs = require('qs');

var isToken;

const app = express();

app.use(express.static('static'));
const cors = require('cors');
const corsOptions ={
    origin:'*', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'x-www-form-urlencoded, Origin, X-Requested-With, Content-Type, Accept, Authorization, *');
  if (req.method === 'OPTIONS'){
      res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS');
      res.setHeader('Access-Control-Allow-Credentials', true);
      return res.status(200).json({});
  }
  next();
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/static/index.html'));
});

app.get('/contract/', (req, res) => {
  res.sendFile(path.join(__dirname, '/static/contract.html'));
});

app.get('/home/', (req, res) => {
  res.sendFile(path.join(__dirname, '/static/home.html'));
});

// if (isToken) {
//   console.log(" condition..", isToken);
//   app.get('/?token=${isToken}', (req, res) => {
//     res.sendFile(path.join(__dirname, '/static/home.html'));
//   });
// } else {
//   app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, '/static/index.html'));
//   });
// }

app.get('/auth', (req, res) => {
  res.redirect(
    `https://login.microsoftonline.com/d9175438-b0f6-44b4-8909-2340bf7798cc/oauth2/v2.0/authorize?client_id=ef8b28bc-873c-40db-9ab3-9b8bc170edfe&scope=https://graph.microsoft.com/.default&response_type=code`, // `https://github.com/login/oauth/authorize?client_id=${process.env.GITHUB_CLIENT_ID}`,
  );
});

function CheckIfToken() {
  if (isToken) {
    res.redirect(`home/?token=${isToken}`);
  } else {
    res.redirect(`/`);
  }
}

app.get('/oauth-callback', ({
  query: {
    code,
  }
}, res) => {
  const body = {
    client_id: 'ef8b28bc-873c-40db-9ab3-9b8bc170edfe',
    client_secret: 'e5u7Q~obNC6tuLD4fXORuTGYN~knZRlJ1c_G-',
    scope: 'https://graph.microsoft.com/.default',
    grant_type: 'client_credentials'
  };
  console.log('body body:', body);
  const opts = {
    headers: {
      accept: 'application/json'
    }
  };
  // axios
  //   .post('https://login.microsoftonline.com/d9175438-b0f6-44b4-8909-2340bf7798cc/oauth2/v2.0/token', body, opts)
  //   .then((_res) =>  {
  //     // eslint-disable-next-line no-console
  //     console.log('My token:', _res);

  //     res.redirect(`/?token=${token}`);
  //   })
  //   .catch((err) => res.status(500).json({
  //     err: err.message
  //   }));

  axios
    .post('https://login.microsoftonline.com/d9175438-b0f6-44b4-8909-2340bf7798cc/oauth2/v2.0/token', qs.stringify(body))
    .then(token => {
      console.log('My token:', token);
      // res.redirect(`/?token=${token.data.access_token}`);

      isToken = token.data.access_token;
      console.log(" console..", isToken);

      res.redirect(`contract/?token=${token.data.access_token}`);
    }).catch(error => {
      console.log(" console..", isToken);

    });


});
app.listen(8080);
// eslint-disable-next-line no-console
console.log('App listening on port 8080');
